(function () {
	/*dom elements*/
	let post = document.getElementsByClassName('post-date');
	let comment = document.getElementsByClassName('comment-date');
	let sale = document.getElementsByClassName('sale');
	let footer = document.getElementsByClassName('footer-date');

	/*dates*/
	let now = new Date();
	let weekAgo = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7);
	let d = weekAgo.getDate();
	let m = weekAgo.getMonth() + 1;
	let y = weekAgo.getFullYear();

	writeDate(post, buildDate(d, m, y))
	writeDate(footer, now.getFullYear())
	writeDate(sale, buildDate(now.getDate(), now.getMonth() + 1, now.getFullYear()))

	for (let i = 0; i < comment.length; i++) {
		let commentDate = new Date(weekAgo.getFullYear(), weekAgo.getMonth(), Math.round(weekAgo.getDate() + (i * 0.7)))
		let chooseDate = commentDate < now ? commentDate : now;

		comment[i].textContent = buildDate(chooseDate.getDate(), chooseDate.getMonth() + 1, chooseDate.getFullYear())
	}

	function zero(val) {
		return val.toString().length === 1 ? '0' + val : val;
	}

	function buildDate(d, m, y) {
		return [zero(d), zero(m), y,].join('.');
	}

	function writeDate(domElement, dateTime) {
		if (domElement) {
			for (let i = 0; i < domElement.length; i++) {
				domElement[i].textContent = dateTime
			}
		}
	}

})();
