document.querySelector('.cookie__close').addEventListener('click', function f() {
    document.querySelector('.cookie').classList.add('display__none')
})

show_hide('.privacy', '.privacy__open', '.privacy__close')
show_hide('.terms', '.terms__open', '.terms__close')

function show_hide(blockName, btnShowName, btnCloseName) {
    const block = document.querySelector(blockName)
    const btnShow = document.querySelector(btnShowName)
    const btnClose = document.querySelector(btnCloseName)

    btnShow.addEventListener('click', (e) => {
        e.preventDefault()
        block.classList.remove('display__none')
    })

    btnClose.addEventListener('click', () => {
        block.classList.add('display__none')

    })

    window.addEventListener('click',  (e) => {
        if (e.target.classList.contains('modals__item')) {
            block.classList.add('display__none')
        }
    })
}